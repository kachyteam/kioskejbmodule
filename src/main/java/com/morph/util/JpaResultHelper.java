/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.util.List;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Tobi-Morph-PC
 * http://stackoverflow.com/questions/2002993/jpa-getsingleresult-or-null
 * http://stackoverflow.com/questions/25616374/javax-persistence-noresultexception-no-entity-found-for-query-jpql-query
 */
public class JpaResultHelper {

    public static Object getSingleResultOrNull(TypedQuery query) {
        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        } else if (results.size() == 1) {
            return results.get(0);
        }
        throw new NonUniqueResultException();
    }

    public static <T> T getSingleResult(TypedQuery<T> query) {
        query.setMaxResults(1);
        List<T> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }
}
