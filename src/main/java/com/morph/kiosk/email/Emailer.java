/**
 *Class Name: Emailer
 *Project Name: KioskEJBModule
 *Developer: Onyedika Okafor (ookafor@morphinnovations.com)
 *Version Info:
 *Create Date: Mar 8, 2017 3:34:07 PM
 *(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
 *as the author and owner of this file and its contents.
 */
package com.morph.kiosk.email;

import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import javax.ejb.Stateless;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Stateless
public class Emailer {

    private String message;
    private String subject;
    private String recipient;
    private PortalRequest request;
    private String uuid;

    private boolean messageSentStatus;

    public Emailer() {
    }

    public Emailer(String message, String subject, String recipient, String uuid, PortalRequest request) {
        this.message = message;
        this.subject = subject;
        this.recipient = recipient;
        this.request = request;
        this.uuid = uuid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public PortalRequest getRequest() {
        return request;
    }

    public void setRequest(PortalRequest request) {
        this.request = request;
    }

    public boolean isMessageSentStatus() {
        return messageSentStatus;
    }

    public void setMessageSentStatus(boolean messageSentStatus) {
        this.messageSentStatus = messageSentStatus;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    

}
