/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames={"userGroup_id", "accessGroup_id"})})
public class PortalUserGroup_AccessGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private PortalUserGroup userGroup;
    
    @ManyToOne
    private PortalAccessGroup accessGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalUserGroup_AccessGroup)) {
            return false;
        }
        PortalUserGroup_AccessGroup other = (PortalUserGroup_AccessGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup[ id=" + id + " ]";
    }

    public PortalUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public PortalAccessGroup getAccessGroup() {
        return accessGroup;
    }

    public void setAccessGroup(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
    }
    
}
