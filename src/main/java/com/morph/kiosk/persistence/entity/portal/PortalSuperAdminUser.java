/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
*Class Name: PortalSuperAdminUser
*Project Name: KioskEJBModule
*Developer: Tobi Osagbemi (tosagbemi@morphinnovations.com)
*Version Info:
*Create Date: Feb 23, 2017 4:11:26 PM
*(C) Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity.portal;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
public class PortalSuperAdminUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private PortalUser user;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateAdded;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateUpdate;
    
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalSuperAdminUser)) {
            return false;
        }
        PortalSuperAdminUser other = (PortalSuperAdminUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PortalSuperAdminUser[ id=" + id + " ]";
    }


    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public PortalUser getUser() {
        return user;
    }

    public void setUser(PortalUser user) {
        this.user = user;
    }

}

