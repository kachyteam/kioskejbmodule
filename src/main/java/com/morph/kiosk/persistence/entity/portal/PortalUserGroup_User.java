/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import com.morph.kiosk.persistence.entity.KioskUser;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
public class PortalUserGroup_User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    private PortalUserGroup userGroup;
    
    @OneToOne(optional = false )
    private KioskUser user;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    private boolean active;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalUserGroup_User)) {
            return false;
        }
        PortalUserGroup_User other = (PortalUserGroup_User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User[ id=" + id + " ]";
    }

    public PortalUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }
    
}
