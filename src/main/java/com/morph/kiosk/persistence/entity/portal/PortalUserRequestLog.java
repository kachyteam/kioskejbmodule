/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import com.morph.kiosk.persistence.entity.KioskUser;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
public class PortalUserRequestLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private PortalRequest request;

    @ManyToOne
    private KioskUser user;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateSent;

    private String verificationCode;

    @Lob
    @Column
    private String message;
    private String subject;
    private boolean messageStatus;

    private boolean codeStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalUserRequestLog)) {
            return false;
        }
        PortalUserRequestLog other = (PortalUserRequestLog) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "PortalUserRequestLog{" + "id=" + id + ", request=" + request + ", dateSent=" + dateSent + ", verificationCode=" + verificationCode + ", message=" + message + ", subject=" + subject + ", messageStatus=" + messageStatus + ", codeStatus=" + codeStatus + '}';
    }

    public PortalRequest getRequest() {
        return request;
    }

    public void setRequest(PortalRequest request) {
        this.request = request;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public boolean isCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(boolean codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(boolean messageStatus) {
        this.messageStatus = messageStatus;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

}
