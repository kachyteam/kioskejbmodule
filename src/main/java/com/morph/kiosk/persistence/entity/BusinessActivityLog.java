/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 * This class keeps log of update activities made on <code>Business</code>.
 * This class also keeps track of the modifier i.e <code><pre>modifiedBy</pre> KioskUser</code>
 */
@Entity
public class BusinessActivityLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String name;
    private String description;
    
    private Long businessId;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDate;
    
    private boolean status;
    
    @Lob
    @Basic(fetch = LAZY)
    private byte[] logo;
    private String logoPath;
    private String logoName;

    @ManyToOne
    private KioskUser owner;
    
    @ManyToOne
    private KioskUser modifiedby;

    public BusinessActivityLog() {
    }

    
    public BusinessActivityLog(Business business) {
        this.name = business.getName();
        this.description = business.getDescription();
        this.logo = business.getLogo();
        this.createdDate = business.getCreatedDate();
        this.logoName = business.getLogoName();
        this.logoPath = business.getLogoPath();
        this.status = business.isStatus();
        this.owner = business.getOwner();
        this.businessId = business.getId();
        this.modifiedDate = business.getModifiedDate();
    }
    
    
    
    
    
  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BusinessActivityLog)) {
            return false;
        }
        BusinessActivityLog other = (BusinessActivityLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.BusinessActivityLog[ id=" + id + " ]";
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public KioskUser getOwner() {
        return owner;
    }

    public void setOwner(KioskUser owner) {
        this.owner = owner;
    }

    public KioskUser getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(KioskUser modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }
    
}
