/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import com.morph.kiosk.persistence.entity.view.PageAuthorization;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
public class PortalPage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String pageModule;
    @Column(unique = true)
    private String pageOutcome;
    private String pageName;
    private String pageDescription;
    private String pageCssIcon;

    private boolean active;
    private boolean menu;

    private String pageDisplayName;

    @ManyToOne
    private PortalPage parent;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;

    @OneToOne
    private PortalRole role;

    @Enumerated(EnumType.STRING)
    private PageAuthorization.DisplayOption displayOption;

    private boolean component;
    
    private int position;

    public PortalPage() {
    }

    public PortalPage(String pageModule, String pageOutcome, String pageName, String pageCssIcon, boolean active, boolean menu, PortalPage parent, Date dateCreated) {
        this.pageModule = pageModule;
        this.pageOutcome = pageOutcome;
        this.pageName = pageName;
        this.pageCssIcon = pageCssIcon;
        this.active = active;
        this.menu = menu;
        this.parent = parent;
        this.dateCreated = dateCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalPage)) {
            return false;
        }
        PortalPage other = (PortalPage) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    public String getPageModule() {
        return pageModule;
    }

    public void setPageModule(String pageModule) {
        this.pageModule = pageModule;
    }

    public String getPageOutcome() {
        return pageOutcome;
    }

    public void setPageOutcome(String pageOutcome) {
        this.pageOutcome = pageOutcome;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        if (null == pageName || pageName.trim().length() < 1) {
            this.pageName = null;
        } else {
            this.pageName = pageName.trim();
        }
    }

    public String getPageDescription() {
        return pageDescription;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public String getPageCssIcon() {
        return pageCssIcon;
    }

    public void setPageCssIcon(String pageCssIcon) {
        this.pageCssIcon = pageCssIcon;
    }

    public PortalPage getParent() {
        return parent;
    }

    public void setParent(PortalPage parent) {
        this.parent = parent;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isMenu() {
        return menu;
    }

    public void setMenu(boolean menu) {
        this.menu = menu;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalRole getRole() {
        return role;
    }

    public void setRole(PortalRole role) {
        this.role = role;
    }

    public PageAuthorization.DisplayOption getDisplayOption() {
        return displayOption;
    }

    public void setDisplayOption(PageAuthorization.DisplayOption displayOption) {
        this.displayOption = displayOption;
    }

    public boolean isComponent() {
        return component;
    }

    public void setComponent(boolean component) {
        this.component = component;
    }

    //@Transient
    public String constructURL() {
        return "/" + pageModule + "/" + pageName + ".xhtml";
    }

    public String getPageDisplayName() {
        return pageDisplayName;
    }

    public void setPageDisplayName(String pageDisplayName) {
        this.pageDisplayName = pageDisplayName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "PortalPage{" + "id=" + id + ", pageModule=" + pageModule + ", pageOutcome=" + pageOutcome + ", pageName=" + pageName + ", pageDisplayName=" + pageDisplayName + ", position=" + position + '}';
    }

    public enum DisplayOption {
        READONLY("Read Only"),
        RENDERED("Render"),
        HIDE("Hide");

        private final String status;

        private DisplayOption(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

    }
}
