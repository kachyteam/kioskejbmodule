/**
 * Class Name: PaymentTransaction Project Name: KioskEJBModuleV2 Developer:
 * Onyedika Okafor (ookafor@morphinnovations.com) Version Info: Create Date: Aug
 * 24, 2016 4:34:41 PM (C)Morph Innovations Limited 2016. Morph Innovations
 * Limited Asserts its right to be known as the author and owner of this file
 * and its contents.
 */
package com.morph.kiosk.persistence.entity.order;

import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Settlement;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com) This class holds
 * record for payment transaction and their status.
 */
@Entity
public class PaymentTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date tranxDate;

    @ManyToOne
    private KioskUser payer;

    private String transactionRef;

    private Double amount = 0.0;
    
    private Double paymentPlatformCharge = 0.0;

    private Double amountBeforeCharge = 0.0;

    @Enumerated(EnumType.STRING)
    private PaymentTractionStatus tractionStatus;

    private String authCode;

    @ManyToOne
    private PaymentProvider paymentProvider;

    @ManyToOne
    private OrderedItem orderItem;

    @ManyToOne
    private Settlement settlement;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public KioskUser getPayer() {
        return payer;
    }

    public void setPayer(KioskUser payer) {
        this.payer = payer;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public OrderedItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderedItem orderItem) {
        this.orderItem = orderItem;
    }

    public Date getTranxDate() {
        return tranxDate;
    }

    public void setTranxDate(Date tranxDate) {
        this.tranxDate = tranxDate;
    }

    public Double getPaymentPlatformCharge() {
        return paymentPlatformCharge;
    }

    public void setPaymentPlatformCharge(Double paymentPlatformCharge) {
        this.paymentPlatformCharge = paymentPlatformCharge;
    }

    public Double getAmountBeforeCharge() {
        return amountBeforeCharge;
    }

    public void setAmountBeforeCharge(Double amountBeforeCharge) {
        this.amountBeforeCharge = amountBeforeCharge;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentTransaction)) {
            return false;
        }
        PaymentTransaction other = (PaymentTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entities.payment.PaymentTransaction[ id=" + id + " ]";
    }

    public PaymentTractionStatus getTractionStatus() {
        return tractionStatus;
    }

    public void setTractionStatus(PaymentTractionStatus tractionStatus) {
        this.tractionStatus = tractionStatus;
    }

    public enum PaymentTractionStatus {

        PAYMENTFAILED("Payment Failed"),
        PAYMENYSUCCESSFUL("Payment Successful"),
        PAYMENTPENDING("Payment Pending");

        private final String status;

        private PaymentTractionStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

    }

}
