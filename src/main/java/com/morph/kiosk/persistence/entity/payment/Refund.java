/**
*Class Name: Refund
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Feb 20, 2017 10:21:13 AM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity.payment;

import com.morph.kiosk.persistence.entity.Bank;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
public class Refund implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Bank bank;

    private String bankAccountNumber;
    
    @ManyToOne
    private Business business;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date refundedDate;
    
    private String paymentCode;
    
    private Double amount = 0.0;
    
    private Boolean paid;
    
    @ManyToOne
    private OrderedItem orderedItem;
    
    @ManyToOne
    private KioskUser paidBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getRefundedDate() {
        return refundedDate;
    }

    public void setRefundedDate(Date refundedDate) {
        this.refundedDate = refundedDate;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public KioskUser getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(KioskUser paidBy) {
        this.paidBy = paidBy;
    }

    public OrderedItem getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(OrderedItem orderedItem) {
        this.orderedItem = orderedItem;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Refund)) {
            return false;
        }
        Refund other = (Refund) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.payment.Refund[ id=" + id + " ]";
    }

}
