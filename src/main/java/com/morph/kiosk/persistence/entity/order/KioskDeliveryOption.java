/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.order;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"kiosk_id", "deliveryOption"})})
public class KioskDeliveryOption implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //pending implementation
//    @ManyToOne
//    private DeliveryOption deliveryOption;
    @Enumerated(EnumType.STRING)
    private Option deliveryOption;

    @ManyToOne
    private Kiosk kiosk;

    @ManyToOne
    private KioskUser user;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    private String comment;

    private Boolean active;

    private Double deliveryCost = 0.0;

    private boolean hasVariedCost;

    @Transient
    private boolean editable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KioskDeliveryOption)) {
            return false;
        }
        KioskDeliveryOption other = (KioskDeliveryOption) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.order.KioskDeliveryOption[ id=" + id + " ]";
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public KioskUser getUser() {
        return user;
    }

    public void setUser(KioskUser user) {
        this.user = user;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(Double deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public Option getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(Option deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isHasVariedCost() {
        return hasVariedCost;
    }

    public void setHasVariedCost(boolean hasVariedCost) {
        this.hasVariedCost = hasVariedCost;
    }

    public enum Option {
        PICKUP("Pick up Delivery"),
        HOMEDELIVERY("Home Delivery");

        private final String option;

        private Option(String option) {
            this.option = option;
        }

        public String getOption() {
            return option;
        }

    }
}
