/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.view;

import com.morph.kiosk.persistence.entity.portal.PortalPage;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
public class PageAuthorization implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private PortalPage page;
    
    @Enumerated(EnumType.STRING)
    private DisplayOption displayOption;
    
    private boolean component;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PageAuthorization)) {
            return false;
        }
        PageAuthorization other = (PageAuthorization) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.PortalAuthorization[ id=" + id + " ]";
    }

    public PortalPage getPage() {
        return page;
    }

    public void setPage(PortalPage page) {
        this.page = page;
    }

    public DisplayOption getDisplayOption() {
        return displayOption;
    }

    public void setDisplayOption(DisplayOption displayOption) {
        this.displayOption = displayOption;
    }

    public boolean isComponent() {
        return component;
    }

    public void setComponent(boolean component) {
        this.component = component;
    }
    
    
    public enum DisplayOption {
        READONLY("Read Only"), 
        RENDERED("Render"), 
        HIDE("Hide");
        
        private final String status;

        private DisplayOption(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
        
    }
    
}
