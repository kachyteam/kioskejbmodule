/**
*Class Name: ItemSubItemGroup
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Nov 28, 2016 8:16:24 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"mainItem_id", "subItemGroup_id"})})
public class ItemSubItemGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Item mainItem;

    @ManyToOne
    private SubItemGroup subItemGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getMainItem() {
        return mainItem;
    }

    public void setMainItem(Item mainItem) {
        this.mainItem = mainItem;
    }

    public SubItemGroup getSubItemGroup() {
        return subItemGroup;
    }

    public void setSubItemGroup(SubItemGroup subItemGroup) {
        this.subItemGroup = subItemGroup;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemSubItemGroup)) {
            return false;
        }
        ItemSubItemGroup other = (ItemSubItemGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.ItemSubItemGroup[ id=" + id + " ]";
    }

}
