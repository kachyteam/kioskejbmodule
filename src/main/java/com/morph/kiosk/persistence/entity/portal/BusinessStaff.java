/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.KioskUser;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tobi-Morph-PC
 * This table hold the Business Staff Business Level Access Group 
 * and Roles(for custom roles. This can as well be in a different Table of its own)
 */
@Entity
public class BusinessStaff implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne(optional = false)
    private KioskUser staff;
    
    @ManyToOne(optional = false)
    private KioskUser creator;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastModified;
    
    private boolean active;

    public BusinessStaff() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BusinessStaff)) {
            return false;
        }
        BusinessStaff other = (BusinessStaff) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.BusinessStaff[ id=" + id + " ]";
    }

    public KioskUser getStaff() {
        return staff;
    }

    public void setStaff(KioskUser staff) {
        this.staff = staff;
    }

    public KioskUser getCreator() {
        return creator;
    }

    public void setCreator(KioskUser creator) {
        this.creator = creator;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastModified() {
        return dateLastModified;
    }

    public void setDateLastModified(Date dateLastModified) {
        this.dateLastModified = dateLastModified;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
