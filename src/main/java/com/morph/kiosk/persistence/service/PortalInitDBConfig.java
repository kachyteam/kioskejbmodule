/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import com.morph.kiosk.persistence.entity.Bank;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.Settings;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Paystack;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup_Permission;
import com.morph.kiosk.persistence.entity.portal.PortalPage;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalRequestType;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.util.PersistenceConstantUtil;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Startup;

/**
 *
 * @author Tobi-Morph-PC
 */
@Singleton
@LocalBean
@Startup
public class PortalInitDBConfig {

    @EJB
    private PortalPersistence persistence;

    @EJB
    private UserPersistence upersistence;

    @PostConstruct
    public void init() {
        setUpDatabase();
        setUpPortalMenuPage();
        setUpPermission();
        setUpAccessGroupPermissions();
        setUpSettings();
        setUpBank();
        setUpSuperAdmin();
    }

    @PreDestroy
    public void destroy() {
        // Do stuff during webapp's shutdown.
    }

    private void setUpSuperAdmin() {
        KioskUser ku = upersistence.getKioskUserByEmailAddress(PersistenceConstantUtil.SUPER_ADMIN_EMAIL);
        if (ku == null) {
            try {
                ku = new KioskUser();
                ku.setFirstName("Super");
                ku.setSurname("Admin");
                ku.setActive(true);
                ku.setCreatedDate(new Date());
                ku.setDesignation(KioskUser.Designation.ADMINISTRATOR);
                ku.setEmailAddress(PersistenceConstantUtil.SUPER_ADMIN_EMAIL);
                ku.setModifiedDate(new Date());
                ku.setPassword("M0rphTent");
                ku.setPhoneNumber("07032778564");
                ku.setToken("07032778564");

                persistence.create(ku);
            } catch (Exception e) {
                System.err.println("setUpSuperAdmin Error :::: " + e.getLocalizedMessage());
            }
        } else {
           // System.out.println("Skip superAdmin Creation :::::::::::::::");
        }
    }

    private void setUpDatabase() {
        PortalAccessGroup accessGroup = persistence.getPortalAccessGroupByName(PersistenceConstantUtil.BUSINESS_OWNER_ACCESS_GROUP);
        if (accessGroup == null) {
            try {
                accessGroup = new PortalAccessGroup();
                accessGroup.setName(PersistenceConstantUtil.BUSINESS_OWNER_ACCESS_GROUP);
                accessGroup.setDescription(PersistenceConstantUtil.BUSINESS_OWNER_ACCESS_GROUP_DESC);
                accessGroup.setActive(true);
                accessGroup.setDateCreated(new Date());
                persistence.create(accessGroup);
            } catch (Exception e) {
                System.err.println("accessGroup AG Error :::: " + e.getLocalizedMessage());
            }
        } else {
           // System.out.println("Skip accessGroup Creation :::::::::::::::");
        }

        PortalAccessGroup managerAccessGroup = persistence.getPortalAccessGroupByName(PersistenceConstantUtil.BUSINESS_MANAGER_ACCESS_GROUP);
        if (managerAccessGroup == null) {
            try {
                managerAccessGroup = new PortalAccessGroup();
                managerAccessGroup.setName(PersistenceConstantUtil.BUSINESS_MANAGER_ACCESS_GROUP);
                managerAccessGroup.setDescription(PersistenceConstantUtil.BUSINESS_MANAGER_ACCESS_GROUP_DESC);
                managerAccessGroup.setActive(true);
                managerAccessGroup.setDateCreated(new Date());
                persistence.create(managerAccessGroup);
            } catch (Exception e) {
                System.err.println("manager AG Error :::: " + e.getLocalizedMessage());
            }
        } else {
           // System.out.println("Skip managerAccessGroup Creation :::::::::::::::");
        }

        PortalUserGroup boUserGroup = persistence.getPortalUserGroupByUserGroupName(PersistenceConstantUtil.BUSINESS_OWNER_USER_GROUP);
        if (null == boUserGroup) {
            try {
                boUserGroup = new PortalUserGroup();
                boUserGroup.setActive(true);
                boUserGroup.setUserGroupName(PersistenceConstantUtil.BUSINESS_OWNER_USER_GROUP);
                persistence.create(boUserGroup);

                PortalUserGroup_AccessGroup userGroup_AccessGroup = new PortalUserGroup_AccessGroup();
                userGroup_AccessGroup.setAccessGroup(accessGroup);
                userGroup_AccessGroup.setUserGroup(boUserGroup);
                persistence.create(userGroup_AccessGroup);

            } catch (Exception e) {
                System.out.println("boUserGroup Error ::::::::: " + e.getLocalizedMessage());
            }
        } else {
           // System.out.println("Skip boUserGroup Creation :::::::::::::::");
        }

        PortalUserGroup maUserGroup = persistence.getPortalUserGroupByUserGroupName(PersistenceConstantUtil.BUSINESS_MANAGER_USER_GROUP);
        if (null == maUserGroup) {
            try {
                maUserGroup = new PortalUserGroup();
                maUserGroup.setActive(true);
                maUserGroup.setUserGroupName(PersistenceConstantUtil.BUSINESS_MANAGER_USER_GROUP);
                persistence.create(maUserGroup);

                PortalUserGroup_AccessGroup userGroup_AccessGroup = new PortalUserGroup_AccessGroup();
                userGroup_AccessGroup.setAccessGroup(managerAccessGroup);
                userGroup_AccessGroup.setUserGroup(maUserGroup);
                persistence.create(userGroup_AccessGroup);

            } catch (Exception e) {
                System.out.println("Skip maUserGroup Creation ::::::::: " + e.getLocalizedMessage());
            }
        }

        PortalUserGroup attendantUserGroup = persistence.getPortalUserGroupByUserGroupName(PersistenceConstantUtil.BUSINESS_ATTENDANT);
        if (null == attendantUserGroup) {
            try {
                attendantUserGroup = new PortalUserGroup();
                attendantUserGroup.setActive(true);
                attendantUserGroup.setUserGroupName(PersistenceConstantUtil.BUSINESS_ATTENDANT);
                persistence.create(attendantUserGroup);
            } catch (Exception e) {
                System.out.println("Skip attendantUserGroup Creation ::::::::: " + e.getLocalizedMessage());
            }
        }

        PortalUserGroup dispatcherUserGroup = persistence.getPortalUserGroupByUserGroupName(PersistenceConstantUtil.BUSINESS_DISPTACHER);
        if (null == dispatcherUserGroup) {
            try {
                dispatcherUserGroup = new PortalUserGroup();
                dispatcherUserGroup.setActive(true);
                dispatcherUserGroup.setUserGroupName(PersistenceConstantUtil.BUSINESS_DISPTACHER);
                persistence.create(dispatcherUserGroup);
            } catch (Exception e) {
                System.out.println("Skip dispatcherUserGroup Creation ::::::::: " + e.getLocalizedMessage());
            }
        }

        PortalUserGroup logisticsUserGroup = persistence.getPortalUserGroupByUserGroupName(PersistenceConstantUtil.BUSINESS_LOGISTIC_MANAGER);
        if (null == logisticsUserGroup) {
            try {
                logisticsUserGroup = new PortalUserGroup();
                logisticsUserGroup.setActive(true);
                logisticsUserGroup.setUserGroupName(PersistenceConstantUtil.BUSINESS_LOGISTIC_MANAGER);
                persistence.create(logisticsUserGroup);
            } catch (Exception e) {
                System.out.println("Skip logisticsUserGroup Creation ::::::::: " + e.getLocalizedMessage());
            }
        }

        System.out.println("This tab got called :::::::::::::::::::::::::::::::::::::::::: ");

        if (persistence.getActivePortalRequestType().isEmpty()) {
            PortalRequestType requestType = new PortalRequestType();
            requestType.setUserGroup(boUserGroup);
            requestType.setActive(true);
            requestType.setDateCreated(new Date());
            requestType.setName(PersistenceConstantUtil.BUSINESS_OWNER_REQUEST);
            persistence.create(requestType);

        } else {
          //  System.out.println("Skip Request Creation :::::: ");
        }

        if (persistence.getPaystackByName(PersistenceConstantUtil.PAYSTACK_NAME) == null) {
            Paystack paystack = new Paystack();
            paystack.setOwnerEmailAddress(PersistenceConstantUtil.OWNER_EMAIL);
            paystack.setPublicKey(PersistenceConstantUtil.PUBLIC_KEY);
            paystack.setSecretKey(PersistenceConstantUtil.SECRET_KEY);
            paystack.setName(PersistenceConstantUtil.PAYSTACK_NAME);
            paystack.setStatus(Boolean.TRUE);
            persistence.create(paystack);

            if (persistence.getPaymentProviderByClassName("Paystack") == null) {
                PaymentProvider provider = new PaymentProvider();
                provider.setClassName("Paystack");
                provider.setName(PersistenceConstantUtil.PAYSTACK_NAME);
                provider.setCreatedDate(new Date());
                provider.setStatus(Boolean.FALSE);
                provider.setDescription("Provider for Online Payment with Paystack");
                provider.setModifiedDate(new Date());
                provider.setProviderId(paystack.getId());
                persistence.create(provider);
            } else {
               // System.out.println("Skip Paystack Creation :::::: ");
            }

        } else {
          //  System.out.println("Skip Paystack Creation :::::: ");
        }

//        if (persistence.getPaymentProviderByClassName("Paystack") == null) {
//            PaymentProvider provider = new PaymentProvider();
//            provider.setClassName("Paystack");
//            provider.setName("Paystack Payment Plaform");
//            provider.setCreatedDate(new Date());
//            provider.setStatus(Boolean.TRUE);
//            persistence.create(provider);
//        } else {
//            System.out.println("Skip Paystack Creation :::::: ");
//        }
        if (upersistence.getKioskUserByEmailAddress("tobiosa@yahoo.com") == null) {
            KioskUser user = new KioskUser();
            user.setActive(true);
            user.setCreatedDate(new Date());
            user.setEmailAddress("tobiosa@yahoo.com");
            user.setPhoneNumber("08038842111");
            user.setPassword("a");
            user.setFirstName("Tobi");
            user.setSurname("Osagbemi");
            user.setMailingAddress("Chris Maduike Ikoyi Lagos");
            persistence.create(user);
        } else {
           // System.out.println("Skip Kiosk User Creation :::::: ");
        }

        if (upersistence.getKioskUserByEmailAddress("aa@aa.com") == null) {
            KioskUser user = new KioskUser();
            user.setActive(true);
            user.setCreatedDate(new Date());
            user.setEmailAddress("aa@aa.com");
            user.setPhoneNumber("0842111");
            user.setPassword("a");
            user.setFirstName("AA");
            user.setSurname("AA");
            user.setMailingAddress("AA Alabama United State");
            persistence.create(user);

        } else {
           // System.out.println("Skip Kiosk User Creation :::::: ");
        }

        KioskUser user = upersistence.getKioskUserByEmailAddress("tobiosa@yahoo.com");
        if (user != null) {
            if (null == upersistence.getUserGroupByPortalUser(user.getId())) {
                PortalUserGroup_User pugu = new PortalUserGroup_User();
                pugu.setActive(true);
                pugu.setDateCreated(new Date());
                pugu.setUser(user);
                pugu.setUserGroup(boUserGroup);
                upersistence.create(pugu);
            } else {
               // System.out.println("Skipping user group creation :::: ");
            }
        }
        KioskUser muser = upersistence.getKioskUserByEmailAddress("aa@aa.com");
        if (user != null) {
            if (null == upersistence.getUserGroupByPortalUser(muser.getId())) {
                PortalUserGroup_User pugu = new PortalUserGroup_User();
                pugu.setActive(true);
                pugu.setDateCreated(new Date());
                pugu.setUser(muser);
                pugu.setUserGroup(maUserGroup);
                upersistence.create(pugu);
            } else {
              //  System.out.println("Skipping user group creation :::: ");
            }
        }

    }

    private void setUpPortalMenuPage() {
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("businessDetail", "boBusinessDetail");
            if (null == page) {
                page = new PortalPage("business", "boBusinessDetail", "businessDetail", "", true, false, null, new Date());

                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("registerBusiness", "boRegisterBusiness");
            if (null == page) {
                page = new PortalPage("business", "boRegisterBusiness", "registerBusiness", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("manageBusiness", "boManageBusiness");
            if (null == page) {
                page = new PortalPage("business", "boManageBusiness", "manageBusiness", "fa fa-briefcase fa-fw", true, true, null, new Date());
                page.setPageDisplayName("Businesses");
                page.setPosition(1);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("orders", "boOrderPending");
            if (null == page) {
                page = new PortalPage("transaction", "boOrderPending", "orders", "fa fa-money fa-fw", true, true, null, new Date());
                page.setPageDisplayName("Manage Orders");
                page.setPosition(6);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
//        try {
//            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("transactions", "boTransactions");
//            if (null == page) {
//                page = new PortalPage("transaction", "boTransactions", "transactions", "fa fa-money fa-fw", true, true, null, new Date());
//                page.setPageDisplayName("Transactions");
//                page.setPosition(7);
//                persistence.create(page);
//            }
//        } catch (Exception e) {
//            e.getLocalizedMessage();
//        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("registerKiosk", "boRegisterKiosk");
            if (null == page) {
                page = new PortalPage("kiosk", "boRegisterKiosk", "registerKiosk", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("manageKiosk", "boManageKiosk");
            if (null == page) {
                page = new PortalPage("kiosk", "boManageKiosk", "manageKiosk", "fa fa-building-o fa-fw", true, true, null, new Date());
                page.setPageDisplayName("Manage Kiosks");
                page.setPosition(2);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("kioskDetail", "boKioskDetail");
            if (null == page) {
                page = new PortalPage("kiosk", "boKioskDetail", "kioskDetail", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("addItem", "boAddKioskItem");
            if (null == page) {
                page = new PortalPage("kiosk", "boAddKioskItem", "addItem", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("itemDetail", "boItemDetail");
            if (null == page) {
                page = new PortalPage("item", "boItemDetail", "itemDetail", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
//        try {
//            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("manageItem", "boManageItem");
//            if (null == page) {
//                page = new PortalPage("item", "boManageItem", "manageItem", "fa fa-list-alt fa-fw", true, true, null, new Date());
//                page.setPageDisplayName("Manage Items");
//                page.setPosition(3);
//                persistence.create(page);
//            }
//        } catch (Exception e) {
//            e.getLocalizedMessage();
//        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("registerItem", "boRegisterItem");
            if (null == page) {
                page = new PortalPage("item", "boRegisterItem", "registerItem", "", true, false, null, new Date());
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("users", "boUsers");
            if (null == page) {
                page = new PortalPage("user", "boUsers", "users", "fa fa-user fa-fw", true, true, null, new Date());
                page.setPageDisplayName("Manage Staff");
                page.setPosition(7);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("register", "boRegisterUser");
            if (null == page) {
                page = new PortalPage("user", "boRegisterUser", "register", "", true, false, null, new Date());
                page.setPageDisplayName("Register Staff");
                page.setPosition(7);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {
            PortalPage page = persistence.getPortalPagesNameByPageNameAndOutcome("detail", "boUserDetail");
            if (null == page) {
                page = new PortalPage("user", "boUserDetail", "detail", "fa fa-user fa-fw", true, false, null, new Date());
                page.setPageDisplayName("Staff Detail");
                page.setPosition(7);
                persistence.create(page);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }

    private void setUpPermission() {
        String[] permissions = {"CREATE", "READ", "UPDATE", "DELETE"};
        try {
            List<PortalPage> pages = persistence.getAllPages();
           // System.out.println("Pages Count :::::::::::: " + pages.size());
            if (!pages.isEmpty()) {
                for (PortalPage page : pages) {
                    for (String s : permissions) {
                        String permissionName = page.getPageModule() + "_" + page.getPageName() + ":" + s;
                        if (null == persistence.getPortalPermissionByPermissionName(permissionName)) {
                            PortalPermission permission = new PortalPermission();
                            permission.setActive(true);
                            permission.setDateCreated(new Date());
                            permission.setPage(page);
                            permission.setPermissionName(permissionName);
                            persistence.create(permission);
                        } else {
                         //   System.out.println("Skip Portal Permission Creation ::::::: ");
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("setUpPermission ::::::::: " + e.getLocalizedMessage());
        }
    }

    //Add Pages to Access Group
    private void setUpAccessGroupPermissions() {
        try {
            PortalAccessGroup group = persistence.getPortalAccessGroupByName(PersistenceConstantUtil.BUSINESS_OWNER_ACCESS_GROUP);
            List<PortalPermission> permissions = persistence.getAllPortalPermission();
            for (PortalPermission permission : permissions) {

                if (null == persistence.getAccessGroupPermissionByPermissionAndAccessGroup(permission.getId(), group.getId())) {
                    try {
                        PortalAccessGroup_Permission group_Permission = new PortalAccessGroup_Permission();
                        group_Permission.setAccessGroup(group);
                        group_Permission.setActive(true);
                        group_Permission.setPermission(permission);
                        group_Permission.setDateCreated(new Date());
                        persistence.create(group_Permission);
                    } catch (Exception e) {
                        System.err.println("PortalAccessGroup_Permission Error :::::  " + e.getLocalizedMessage());
                    }
                } else {
                   // System.err.println("Skipping PortalAccessGroup_Permission Creation...");
                }
            }
        } catch (Exception e) {
            System.err.println("setUpAccessGroupPermissions Error ::::: " + e.getLocalizedMessage());
        }
    }

    private void setUpSettings() {
        try {
            Settings setting = persistence.getSettingByName(PersistenceConstantUtil.TIME_BEFORE_PAGE_RELOAD);
            if (setting == null) {
                setting = new Settings();
                setting.setDescription(PersistenceConstantUtil.TIME_BEFORE_PAGE_RELOAD);
                setting.setName(PersistenceConstantUtil.TIME_BEFORE_PAGE_RELOAD);
                setting.setSettingValue("10");
                persistence.create(setting);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        try {
            Settings setting = persistence.getSettingByName(PersistenceConstantUtil.PAYSTACK_PERCENTAGE_CHARGE);
            if (setting == null) {
                setting = new Settings();
                setting.setDescription(PersistenceConstantUtil.PAYSTACK_PERCENTAGE_CHARGE);
                setting.setName(PersistenceConstantUtil.PAYSTACK_PERCENTAGE_CHARGE);
                setting.setSettingValue("1.5");
                persistence.create(setting);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        try {
            Settings setting = persistence.getSettingByName(PersistenceConstantUtil.PAYSTACK_FLAT_CHARGE);
            if (setting == null) {
                setting = new Settings();
                setting.setDescription(PersistenceConstantUtil.PAYSTACK_FLAT_CHARGE);
                setting.setName(PersistenceConstantUtil.PAYSTACK_FLAT_CHARGE);
                setting.setSettingValue("100");
                persistence.create(setting);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }

    private void setUpBank() {
        for (PersistenceConstantUtil.Banks bs : PersistenceConstantUtil.Banks.values()) {
            Bank bank = persistence.getBankByName(bs.getName());
            try {
                if (bank == null) {
                    Bank b = new Bank();
                    b.setLongCode(bs.getLongCode());
                    b.setShortCode(bs.getShortCode());
                    b.setName(bs.getName());
                    persistence.create(b);
                }
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
        }
    }
}
