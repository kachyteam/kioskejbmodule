/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import com.morph.kiosk.persistence.entity.Bank;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.BusinessAccount;
import com.morph.kiosk.persistence.entity.BusinessActivityLog;
import com.morph.kiosk.persistence.entity.DistanceCost;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.ItemImage;
import com.morph.kiosk.persistence.entity.ItemSubItem;
import com.morph.kiosk.persistence.entity.ItemSubItemGroup;
import com.morph.kiosk.persistence.entity.ItemVariation;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskActivityLog;
import com.morph.kiosk.persistence.entity.KioskItem;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.PushRegister;
import com.morph.kiosk.persistence.entity.Settings;
import com.morph.kiosk.persistence.entity.SubItem;
import com.morph.kiosk.persistence.entity.SubItemGroup;
import com.morph.kiosk.persistence.entity.SubitemgroupSubitem;
import com.morph.kiosk.persistence.entity.VariationSubItemCost;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.CouponBatch;
import com.morph.kiosk.persistence.entity.order.CouponLog;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Paystack;
import com.morph.kiosk.persistence.entity.payment.Refund;
import com.morph.kiosk.persistence.entity.payment.Settlement;
import com.morph.kiosk.persistence.entity.payment.Split;
import com.morph.kiosk.persistence.entity.payment.SplitLog;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.entity.portal.PasswordChange;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroupPortalRole;
import com.morph.kiosk.persistence.entity.portal.PortalAccessGroup_Permission;
import com.morph.kiosk.persistence.entity.portal.PortalPage;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalRequestType;
import com.morph.kiosk.persistence.entity.portal.PortalRole;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserRequestLog;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
@Stateless
public class PortalPersistence extends PersistenceImp {

    @Override
    @PersistenceContext(unitName = "KioskEJBModule_V3_PU")
    public void setEm(EntityManager em) {
        super.setEm(em);
    }

    // @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<PortalRole> getActivePortalRole() {
        try {
            return getEm().createQuery("SELECT o from PortalRole o WHERE o.active =1", PortalRole.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRole> getDeactivatedPortalRole() {
        try {
            return getEm().createQuery("SELECT o from PortalRole o WHERE o.active =0", PortalRole.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalRole getPortalRoleByName(String name) {
        PortalRole p;
        try {
            p = getEm().createQuery("SELECT o FROM PortalRole o WHERE o.name=:name", PortalRole.class).setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            p = null;
            e.getLocalizedMessage();
        }
        return p;
    }

    public List<PortalAccessGroup> getActivePortalAccessGroups() {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroup o WHERE o.active =1", PortalAccessGroup.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public SubItem mergeSubItem(SubItem subItem) {
        return getEm().merge(subItem);
    }

    public Item mergeItem(Item item) {
        return getEm().merge(item);
    }

    public List<PortalAccessGroup> getDeactivatedPortalAccessGroups() {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroup o WHERE o.active =0", PortalAccessGroup.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalAccessGroup getPortalAccessGroupByName(String name) {
        PortalAccessGroup p;
        try {
            p = getEm().createQuery("SELECT o FROM PortalAccessGroup o WHERE o.name=:name", PortalAccessGroup.class)
                    .setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            p = null;
            e.getLocalizedMessage();
        }
        return p;
    }

    public List<PortalAccessGroupPortalRole> getAccessGroupPortalRolesByAccessGroupId(Long accessId) {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroupPortalRole o WHERE o.accessGroup.id =:accessId", PortalAccessGroupPortalRole.class)
                    .setParameter("accessId", accessId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalAccessGroupPortalRole getPortalAccessGroupRoleByRoleIdAndAccessId(Long roleId, Long accessId) {
        PortalAccessGroupPortalRole p;
        try {
            p = getEm().createQuery("SELECT o FROM PortalAccessGroupPortalRole o WHERE o.role.id=:roleId AND o.accessGroup.id=:accessId", PortalAccessGroupPortalRole.class)
                    .setParameter("roleId", roleId).setParameter("accessId", accessId)
                    .getSingleResult();
        } catch (Exception e) {
            p = null;
            e.getLocalizedMessage();
        }
        return p;
    }

    public PortalRequestType getPortalRequestTypeByName(String name) {
        PortalRequestType p;
        try {
            p = getEm().createQuery("SELECT o FROM PortalRequestType o WHERE o.name=:name", PortalRequestType.class)
                    .setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            p = null;
            e.getLocalizedMessage();
        }
        return p;
    }

    public List<PortalRequestType> getActivePortalRequestType() {
        try {
            return getEm().createQuery("SELECT o FROM PortalRequestType o WHERE o.active =1", PortalRequestType.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRequestType> getDeactivatedPortalRequestType() {
        try {
            return getEm().createQuery("SELECT o FROM PortalRequestType o WHERE o.active=0", PortalRequestType.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRequest> getPortalRequest() {
        try {
            return getEm().createQuery("SELECT o FROM PortalRequest o WHERE o.active=1", PortalRequest.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRequest> getPortalDynamicAccessRequest(PortalRequest.RequestStatus status) {
        try {
            return getEm().createQuery("SELECT o FROM PortalRequest o WHERE o.status=:status", PortalRequest.class)
                    .setParameter("status", status).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getPortalDynamicAccessRequestCount(PortalRequest.RequestStatus status) {
        return getEm().createQuery("SELECT COUNT(o) FROM PortalRequest o WHERE o.status=:status", Long.class)
                .setParameter("status", status).getSingleResult();
    }

    public List<PortalRequest> getPortalRequestsByKioskUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM PortalRequest o WHERE o.user.id =:userId", PortalRequest.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Business getBusinessByName(String name) {
        Business b;
        try {
            b = getEm().createQuery("SELECT o FROM Business o WHERE o.name=:name", Business.class).setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            b = null;
        }
        return b;
    }

    public List<Business> geAllActiveBusinessByUser(Long ownerId) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:id AND o.status = TRUE", Business.class)
                    .setParameter("id", ownerId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> geAllDisabledBusinessByUser(Long ownerId) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:id AND o.status =0", Business.class)
                    .setParameter("id", ownerId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> geAllBusinessByUser(Long ownerId) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:id", Business.class)
                    .setParameter("id", ownerId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Business getBusinessByUserIdAndBusinessId(Long userId, Long businessId) {
        Business business;
        try {
            business = getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:userId AND o.id =:businessId", Business.class)
                    .setParameter("userId", userId)
                    .setParameter("businessId", businessId).getSingleResult();
        } catch (Exception e) {
            business = null;
            e.getLocalizedMessage();
        }
        return business;
    }

    public List<Business> geAllActiveBusiness() {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.status=1", Business.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> geAllDeacctiveBusiness() {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.status=0", Business.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Business getActiveBusinessByUserIdAndBusinessId(Long userId, Long businessId) {
        Business business;
        try {
            business = getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:userId AND o.id =:businessId AND o.status=1", Business.class)
                    .setParameter("userId", userId)
                    .setParameter("businessId", businessId).getSingleResult();
        } catch (Exception e) {
            business = null;
            e.getLocalizedMessage();
        }
        return business;
    }

    public Kiosk getKioskByKioskName(String kioskName, Long businessId) {
        Kiosk k;
        try {
            k = getEm().createQuery("SELECT o FROM Kiosk o WHERE o.name =:kioskName AND o.business.id=:businessId", Kiosk.class).setParameter("kioskName", kioskName)
                    .setParameter("businessId", businessId).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            k = null;
        }
        return k;
    }

    public Kiosk getKioskByKioskAndBusiness(Long kquery, Long bquery) {
        Kiosk kiosk;
        try {
            kiosk = getEm().createQuery("SELECT o FROM Kiosk o WHERE o.id =:kquery AND o.business.id =:bquery", Kiosk.class)
                    .setParameter("kquery", kquery)
                    .setParameter("bquery", bquery)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            kiosk = null;
        }
        return kiosk;
    }

    public List<Kiosk> getActiveKioskByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status =1 AND o.business.id =:bquery", Kiosk.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getDisabledKioskByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status =0 AND o.business.id =:bquery", Kiosk.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getActiveKioskByUser(KioskUser owner) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status = TRUE AND o.owner =:owner", Kiosk.class)
                    .setParameter("owner", owner).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getKioskByUser(KioskUser owner) {
        try {
            return getEm().createQuery("SELECT o.kiosk FROM BusinessStaffRole o WHERE o.businessStaff.staff =:owner", Kiosk.class)
                    .setParameter("owner", owner).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getActiveKioskByUser(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status = TRUE AND o.owner.id =:id", Kiosk.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getDisabledKioskByUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status =0 AND o.owner.id =:userId", Kiosk.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItemGroup> getSubItemGroupByOwner(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItemGroup o WHERE o.owner.id =:userId ORDER BY o.business", SubItemGroup.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public SubitemgroupSubitem getSubitemgroupSubitem(Long subItemGroupId, Long subItemId) {
        SubitemgroupSubitem ss;
        try {
            ss = getEm().createQuery("SELECT o FROM SubitemgroupSubitem o WHERE o.subItem.id =:subItemId AND o.subItemGroup.id =:subItemGroupId", SubitemgroupSubitem.class)
                    .setParameter("subItemGroupId", subItemGroupId)
                    .setParameter("subItemId", subItemId)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            ss = null;
        }
        return ss;
    }

    public List<Item> getActiveItemByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.status =1 AND o.business.id =:bquery", Item.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getDisabledItemByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.status =0 AND o.business.id =:bquery", Item.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getActiveSubItemByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.active =1 AND o.business.id =:bquery", SubItem.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItemGroup> getActiveSubItemGroupByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM SubItemGroup o WHERE o.active =1 AND o.business.id =:bquery", SubItemGroup.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getDisabledSubItemByBusiness(Long bquery) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.active =0 AND o.business.id =:bquery", SubItem.class)
                    .setParameter("bquery", bquery).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getActiveItemByUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.status = 1 AND o.owner.id =:userId ORDER BY o.business", Item.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getDisableItemByUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.status =0 AND o.owner.id =:userId ORDER BY o.business", Item.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getActiveSubItemByUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.active=1 AND o.owner.id=:userId ORDER BY o.business", SubItem.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getDisableSubItemByUser(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.active =0 AND o.owner.id =:userId ORDER BY o.business", SubItem.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Item getItemByItemAndUserId(Long itemId, Long userId) {
        Item i;
        try {
            i = getEm().createQuery("SELECT o FROM Item o WHERE o.id=:itemId AND o.owner.id=:userId ", Item.class)
                    .setParameter("itemId", itemId)
                    .setParameter("userId", userId)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            i = null;
        }
        return i;
    }

    public List<ItemVariation> getAllItemVariationByItemId(Long itemId) {
        try {
            return getEm().createQuery("SELECT o FROM ItemVariation o WHERE o.item.id =:itemId", ItemVariation.class)
                    .setParameter("itemId", itemId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<VariationSubItemCost> getSubItemCostByItemVariation(ItemVariation itemVariation) {
        try {
            return getEm().createQuery("SELECT o FROM VariationSubItemCost o WHERE o.itemVariation=:itemVariation", VariationSubItemCost.class)
                    .setParameter("itemVariation", itemVariation).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public VariationSubItemCost getVariationCostBySubItem(SubItem si, Item i) {
        VariationSubItemCost cost;
        try {
            cost = getEm().createQuery("SELECT o FROM VariationSubItemCost o WHERE o.mainItem=:i AND o.subItem=:si", VariationSubItemCost.class)
                    .setParameter("si", si)
                    .setParameter("i", i)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            cost = null;
        }
        return cost;
    }

    public VariationSubItemCost getVariationCostBySubItemAndVarition(SubItem si, ItemVariation iv) {
        VariationSubItemCost cost;
        try {
            cost = getEm().createQuery("SELECT o FROM VariationSubItemCost o WHERE o.itemVariation=:iv AND o.subItem=:si AND o.active = TRUE", VariationSubItemCost.class)
                    .setParameter("si", si)
                    .setParameter("iv", iv)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            cost = null;
        }
        return cost;
    }

    public List<SubItem> getAllBusinessSubItems(Long businessId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.business.id =:businessId AND o.active =1", SubItem.class)
                    .setParameter("businessId", businessId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getAllBusinessSubItemsByOwner(Long userId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItem o WHERE o.owner.id =:userId AND o.active =1", SubItem.class)
                    .setParameter("userId", userId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItemGroup> getAllItemSubItemGroups(Long itemId) {
        try {
            return getEm().createQuery("SELECT o.subItemGroup FROM ItemSubItemGroup o WHERE o.mainItem.id=:itemId", SubItemGroup.class)
                    .setParameter("itemId", itemId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItemGroup> getAllUniqueItemSubItemGroups(Long itemId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItemGroup o WHERE o.item.id=:itemId", SubItemGroup.class)
                    .setParameter("itemId", itemId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getAllVariationSubItems(ItemVariation iv) {
        try {
            return getEm().createQuery("SELECT o.subItem FROM VariationSubItemCost o WHERE o.itemVariatio =:itemVariation AND o.active = TRUE", SubItem.class)
                    .setParameter("itemVariation", iv).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getAllItemSubItems(Long itemId) {
        try {
            return getEm().createQuery("SELECT o.subItem FROM ItemSubItem o WHERE o.mainItem.id =:itemId", SubItem.class)
                    .setParameter("itemId", itemId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubitemgroupSubitem> getSubitemgroupSubitemsBySubItemGroup(SubItemGroup itemGroup) {
        try {
            return getEm().createQuery("SELECT o FROM SubitemgroupSubitem o WHERE o.subItemGroup=:itemGroup", SubitemgroupSubitem.class)
                    .setParameter("itemGroup", itemGroup).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public ItemSubItemGroup geItemSubItemGroupByItemSubItem(Long itemId, Long subItemGroupId) {
        ItemSubItemGroup i;
        try {
            i = getEm().createQuery("SELECT o FROM ItemSubItemGroup o WHERE o.mainItem.id=:itemId AND o.subItemGroup.id=:subItemGroupId", ItemSubItemGroup.class)
                    .setParameter("itemId", itemId)
                    .setParameter("subItemGroupId", subItemGroupId)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            i = null;
        }
        return i;
    }

    public ItemSubItem geItemSubItemByItemSubItem(Long itemId, Long subItemId) {
        ItemSubItem i;
        try {
            i = getEm().createQuery("SELECT o FROM ItemSubItem o WHERE o.mainItem.id=:itemId AND o.subItem.id=:subItemId ", ItemSubItem.class)
                    .setParameter("itemId", itemId)
                    .setParameter("subItemId", subItemId)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            i = null;
        }
        return i;
    }

    public List<ItemImage> getItemImagesByItemId(Long itemId) {
        try {
            return getEm().createQuery("SELECT o FROM ItemImage o WHERE o.item.id =:itemId", ItemImage.class).setParameter("itemId", itemId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getAllItemByBusinessId(Long businessId) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.business.id =:businessId", Item.class)
                    .setParameter("businessId", businessId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getItemFromKioskItemByKioskId(Long id) {
        try {
            return getEm().createQuery("SELECT o.item FROM KioskItem o WHERE o.kiosk.id=:id", Item.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public KioskItem getKioskItemByKioskAndItem(Long kioskId, Long itemId) {
        KioskItem ki;
        try {
            ki = getEm().createQuery("SELECT O FROM KioskItem O WHERE O.kiosk.id =:kioskId AND O.item.id =:itemId", KioskItem.class)
                    .setParameter("kioskId", kioskId)
                    .setParameter("itemId", itemId)
                    .getSingleResult();
        } catch (Throwable t) {
            System.err.println("getKioskItemByKioskAndItem return null :::::::::::::::::::: " + t.getLocalizedMessage());
            ki = null;
        }
        return ki;
    }

    public List<KioskItem> getKioskItemByKioskId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM KioskItem o WHERE o.status = TRUE AND o.item.status = TRUE AND o.kiosk.id =:id", KioskItem.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getItemSubItems(Item mainItem) {
        return getEm().createQuery("SELECT o.subItem FROM ItemSubItem o WHERE o.mainItem =:mainItem AND o.subItem.active = TRUE", SubItem.class)
                .setParameter("mainItem", mainItem)
                .getResultList();
    }

    public List<KioskItem> getKioskItemByName(String name) {
        return getEm().createQuery("SELECT o FROM KioskItem o WHERE o.item.status = TRUE AND o.item.name LIKE '%" + name + "%' OR o.item.description LIKE '%" + name + "%'", KioskItem.class)
                .getResultList();
    }

    public List<Kiosk> getKiosksByItemName(String name) {
        return getEm().createQuery("SELECT o.kiosk FROM KioskItem o WHERE o.item.status = TRUE AND o.kiosk.status = TRUE AND o.item.name LIKE :name OR o.item.description LIKE :name", Kiosk.class)
                .setParameter("name", "%" + name + "%")
                .getResultList();
    }

    public List<Kiosk> getAllKitchen() {
        return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status = TRUE AND o.business.status = TRUE", Kiosk.class)
                .getResultList();
    }

    public List<Kiosk> getAllActiveKiosk() {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status = TRUE", Kiosk.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getAllKioskByBusinessId(Long businessId) {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.business.id =:businessId ", Kiosk.class)
                    .setParameter("businessId", businessId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Kiosk> getAllDeactivatedKiosk() {
        try {
            return getEm().createQuery("SELECT o FROM Kiosk o WHERE o.status=0 ", Kiosk.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getAllActiveItems() {
        try {
            return getEm().createQuery("SELECT o FROM Item o", Item.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getAllDeactivatedItems() {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.status=0 ", Item.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getAllDeactivatedItemsByBusinessId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.business.id =:id AND o.status =0  ", Item.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Item> getAllActiveItemsByBusinessId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Item o WHERE o.business.id =:id AND o.status =1  ", Item.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> getAllActiveBusinessByUserId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.status =1 AND o.owner.id =:id", Business.class).setParameter("id", id).getResultList();

        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> getAllBusinessByUserId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id =:id", Business.class)
                    .setParameter("id", id).getResultList();

        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Business> getAllDeactivatedBusinessByUserId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM Business o WHERE o.status =1 AND o.owner.id =:id", Business.class).setParameter("id", id).getResultList();

        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentProvider> getAllPaymentProviders() {

        try {
            return getEm().createQuery("SELECT o FROM PaymentProvider o", PaymentProvider.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Split> getAllSplits() {

        try {
            return getEm().createQuery("SELECT o FROM Split o", Split.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Split getSplitByName(String splitName) {
        Split split;
        try {
            split = getEm().createQuery("SELECT o FROM Split o WHERE o.name =:splitName", Split.class)
                    .setParameter("splitName", splitName)
                    .getSingleResult();
            System.out.println("getSplitByName ::::: " + split);
        } catch (Throwable t) {
            t.getLocalizedMessage();
            split = null;
        }
        return split;
    }

    public BusinessAccount getBusinessAccountByBankAndAccountNumber(String accountNumber, Bank bank) {
        BusinessAccount account;
        try {
            account = getEm().createQuery("SELECT o FROM BusinessAccount o WHERE o.bankAccountNumber =:accountNumber AND o.bank =:bank", BusinessAccount.class)
                    .setParameter("bank", bank)
                    .setParameter("accountNumber", accountNumber)
                    .getSingleResult();
            System.out.println("getBusinessAccountByBankAndAccountNumber ::::: " + account);
        } catch (Throwable t) {
            t.getLocalizedMessage();
            account = null;
        }
        return account;
    }

    public List<PortalRole> getAllPortalRoleWithoutPage() {
        try {
            //getEm().createNativeQuery("")
            return getEm().createQuery("SELECT o from PortalRole o WHERE o.id NOT IN (SELECT e.role.id FROM PortalPage e)", PortalRole.class).getResultList();
//            String query = "SELECT * FROM portalrole WHERE portalrole.id NOT IN (SELECT portalpage.role_id FROM portalpage)";
//            System.out.println(query + " ------ " + getEm().createNativeQuery(query, PortalRole.class).getResultList().size());
//            return getEm().createNativeQuery("SELECT * FROM portalrole WHERE portalrole.id NOT IN (SELECT portalpage.role_id FROM portalpage)", PortalRole.class).getResultList();
        } catch (Exception e) {
            System.err.println("getAllPortalRoleWithoutPage return empty :::::::::::::::::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRole> getAllPortalRoles() {
        try {
            return getEm().createQuery("SELECT o from PortalRole o WHERE o.active =1", PortalRole.class).getResultList();
        } catch (Exception e) {
            System.err.println("getAllPortalRoles return empty :::::::::::::::::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRole> getPortalRoleByUserId(Long userId) {
        try {
            return getEm().createQuery("SELECT o.role FROM KioskUserRole o WHERE o.user.id =:id")
                    .setParameter("id", userId)
                    .getResultList();
        } catch (Exception e) {
            System.err.println("getPortalRoleByUserId return empty :::::::::::::::::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalRole> getAllPortalUserPortalRoleByUserId(Long id) {
        try {
            return getEm().createQuery("SELECT o.role from PortalUserRole o WHERE o.user.id =:id").setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getAllKioskUserPortalRoleByUserId return empty :::::::::::::::::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;

        }
    }

    public void removePortalRoleByRoleIdAndUserId(Long roleID, Long userID) {
        try {
            PortalUser userRole = (PortalUser) getEm()
                    .createQuery("SELECT o FROM PortalUserRole o WHERE o.role.id =:roleID AND o.user.id =:userID")
                    .setParameter("roleID", roleID)
                    .setParameter("userID", userID).getSingleResult();

            //getEm().createQuery("SELECT o from PortalPage o WHERE o.role.id =:id")
            delete(userRole);
        } catch (Exception e) {
            System.err.println("removePortalRoleByRoleIdAndUserId return error :::::::::::::::::::: " + e.getLocalizedMessage());
        }
    }

    public PortalRequest getUserRequest(Long id) {
        PortalRequest pr;
        try {

            pr = getEm().createQuery("SELECT o FROM PortalRequest o WHERE o.user.id=:id", PortalRequest.class)
                    .setParameter("id", id)
                    .getSingleResult();

        } catch (Exception e) {
            pr = null;
            e.getLocalizedMessage();
        }
        return pr;
    }

    public PortalAccessGroup getPortalAccessGroupByPortalRequestType(Long id) {
        PortalAccessGroup p;
        try {
            p = getEm().createQuery("", PortalAccessGroup.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            p = null;
        }

        return p;
    }

    public List<PortalPage> getAllPages() {
        try {
            return getEm().createQuery("SELECT o FROM PortalPage o WHERE o.active=1", PortalPage.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<String> getAllPagesName() {
        try {
            return getEm().createQuery("SELECT o.pageName FROM PortalPage o WHERE o.active=1 AND o.component=0", String.class).getResultList();
        } catch (Exception e) {
            System.err.println("getAllPagesName return null :::::::::::::::::::: ");
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public String getPortalPagesNameByPageName(String pageName) {
        String s = null;
        try {
            s = getEm().createQuery("SELECT o.pageName FROM PortalPage o WHERE o.active=1 AND o.component=0 AND o.pageName=:pageName", String.class)
                    .setParameter("pageName", pageName).getSingleResult();
        } catch (Exception e) {
            System.err.println("getPortalPagesNameByPageName return null :::::::::::::::::::: ");
            e.getLocalizedMessage();
            //s=null;
        }
        return s;
    }

    public List<PortalPage> getAllComponent() {
        try {
            return getEm().createQuery("SELECT o FROM PortalPage o WHERE O.component=1 AND o.active=1", PortalPage.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<String> getAllComponentName() {
        try {
            return getEm().createQuery("SELECT o.pageName FROM PortalPage o WHERE o.component=1 AND o.active=1", String.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalPage getPortalPagesByRole(Long roleId) {
        PortalPage pp;
        try {
            pp = getEm().createQuery("SELECT o from PortalPage o WHERE o.role.id =:id", PortalPage.class)
                    .setParameter("id", roleId)
                    .getSingleResult();
        } catch (Throwable t) {
            System.err.println("getPortalPagesByRole return null :::::::::::::::::::: ");
            t.getLocalizedMessage();
            pp = null;
        }
        return pp;
    }

    public String getPortalPagesNameByRole(Long roleId) {
        String pp;
        try {
            pp = getEm().createQuery("SELECT o.pageName from PortalPage o WHERE o.role.id =:id AND o.component=0 AND o.active=1", String.class).getSingleResult();

        } catch (Throwable t) {
            System.err.println("getPortalPagesNameByRole return null :::::::::::::::::::: ");
            t.getLocalizedMessage();
            pp = null;
        }
        return pp;
    }

    public PortalUserRequestLog getPortalUserRequestLogByIdAndActCoce(Long userId, String verificationCode) {
        PortalUserRequestLog log;
        try {
            System.out.println("Request Log Parameter ::::::: " + userId + " :::: " + verificationCode);
            log = getEm().createQuery("SELECT o FROM PortalUserRequestLog o WHERE o.user.id =:userId AND o.verificationCode =:verificationCode AND o.codeStatus = 1", PortalUserRequestLog.class)
                    .setParameter("userId", userId)
                    .setParameter("verificationCode", verificationCode)
                    .getSingleResult();
        } catch (Exception e) {
            System.err.println("getPortalUserRequestLogByIdAndActCoce return null :::: ");
            e.getLocalizedMessage();
            log = null;
        }
        return log;
    }

    public List<KioskDeliveryOption> getKioskDeliveryOptionsByKioskId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM KioskDeliveryOption o WHERE o.kiosk.id =:id", KioskDeliveryOption.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<KioskPaymentOption> getKioskPaymentOptionsByKioskId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM KioskPaymentOption o WHERE o.kiosk.id =:id", KioskPaymentOption.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Cart getCartByBatchId(String cartId) {
        Cart c = null;
        try {
            //c = getEm().createQuery("", Cart.class).setParameter("", cartId).get
        } catch (Exception e) {
        }
        return c;
    }

    public List<OrderedItem> myOrders(KioskUser kioskUser) {
        return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.orderedBy =:user", OrderedItem.class)
                .setParameter("user", kioskUser)
                .getResultList();
    }

    public KioskUser getUserByToken(String token) {
        KioskUser user;
        try {
            user = getEm().createQuery("SELECT o FROM KioskUser o WHERE o.token =:token", KioskUser.class)
                    .setParameter("token", token)
                    .getSingleResult();
            return user;
        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public PasswordChange getPasswordChangeByCode(String code) {
        PasswordChange pc;
        try {
            pc = getEm().createQuery("SELECT o FROM PasswordChange o WHERE o.changeCode=:code", PasswordChange.class)
                    .setParameter("code", code)
                    .getSingleResult();
            return pc;
        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public Coupon getCouponByCode(String couponCode) {
        Coupon c;
        try {
            c = getEm().createQuery("SELECT o FROM Coupon o WHERE o.code=:couponCode", Coupon.class)
                    .setParameter("couponCode", couponCode)
                    .getSingleResult();
            return c;
        } catch (Exception e) {
            return null;
        }
    }

    public CouponLog getCouponLogByOrderIdAndCouponId(Long orderId, Long couponId) {
        CouponLog c;
        try {
            c = getEm().createQuery("SELECT o FROM CouponLog o WHERE o.couponId=:couponId AND o.orderId=:orderId", CouponLog.class)
                    .setParameter("orderId", orderId)
                    .setParameter("couponId", couponId)
                    .getSingleResult();
            return c;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isCouponValid(String couponCode, Long businessId, Long userId) {
        boolean isValid = false;
        Coupon c = null;
        try {
            c = getEm().createQuery("SELECT o FROM Coupon o WHERE o.code=:couponCode AND o.active = TRUE AND o.used = FALSE AND o.couponBatch.active = TRUE AND o.couponBatch.business.id=:businessId", Coupon.class)
                    .setParameter("couponCode", couponCode)
                    .setParameter("businessId", businessId)
                    .getSingleResult();
        } catch (NoResultException e) {
        }

        if (c != null) {
            CouponLog cl = null;
            try {
                cl = getEm().createQuery("SELECT o FROM CouponLog o WHERE o.couponId=:couponId AND o.userId=:userId", CouponLog.class)
                        .setParameter("couponId", c.getId())
                        .setParameter("userId", userId)
                        .getSingleResult();
            } catch (NoResultException e) {
            }

            if (cl == null) {
                if (new Date().getTime() < c.getCouponBatch().getDateExpired().getTime()) {
                    List<CouponLog> logs = getEm().createQuery("SELECT o FROM CouponLog o WHERE o.couponId =:couponId", CouponLog.class)
                            .setParameter("couponId", c.getId())
                            .getResultList();
                    if (c.getCouponBatch().isMultipleUser()) {
                        if (logs.size() < c.getCouponBatch().getUsageLimit()) {
                            isValid = true;
                        }
                    } else {
                        if (logs.size() < c.getCouponBatch().getUsageLimit()) {
                            isValid = true;
                        }
                    }
                }
            }
        }
        return isValid;
    }

    public boolean isUsageExceeded(String couponCode) {
        boolean exceeded = false;
        try {
            Coupon c = getEm().createQuery("SELECT o FROM Coupon o WHERE o.code=:couponCode", Coupon.class)
                    .setParameter("couponCode", couponCode)
                    .getSingleResult();
            if (c != null) {
                if (c.getCouponBatch().isMultipleUser()) {
                    List<CouponLog> logs = getEm().createQuery("SELECT o FROM CouponLog o WHERE o.couponId =:couponId", CouponLog.class)
                            .setParameter("couponId", c.getId())
                            .getResultList();

                    if (logs.size() >= c.getCouponBatch().getUsageLimit()) {
                        exceeded = true;
                    }
                } else {
                    exceeded = true;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            exceeded = false;
        }
        return exceeded;
    }

    public List<Kiosk> getKiosks(Double lat, Double lon) {
        String sql = "SELECT *, (6371 * acos(cos(radians(" + lat + ")) * cos(radians(latitude)) * cos(radians(longitude) - radians(" + lon + ")) + sin(radians(" + lat + ")) * sin(radians(latitude)))) AS distance FROM Kiosk HAVING distance < 3 ORDER BY distance LIMIT 0 , 20";
        List<Kiosk> kiosks = getEm().createNativeQuery(sql, Kiosk.class).getResultList();
        return kiosks;
    }

    public ItemImage getItemImage(Item item) {
        ItemImage ii;
        try {
            ii = getEm().createQuery("SELECT o FROM ItemImage o WHERE o.item =:item", ItemImage.class)
                    .setParameter("item", item)
                    .getSingleResult();
            return ii;
        } catch (Exception e) {
            return null;
        }
    }

    public ItemImage getItemImageByItemId(Long itemId) {
        ItemImage ii;
        try {
            ii = getEm().createQuery("SELECT o FROM ItemImage o WHERE o.item.id =:itemId", ItemImage.class)
                    .setParameter("itemId", itemId)
                    .getSingleResult();
            return ii;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Cart> getAllCartsByBatchId(String batchId) {
        try {
            return getEm().createQuery("SELECT o FROM Cart o WHERE o.batchId =:batchId ORDER BY o.createdDate ASC", Cart.class)
                    .setParameter("batchId", batchId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItemCart> getAllSubItemCartByCartId(Long cartId) {
        try {
            return getEm().createQuery("SELECT o FROM SubItemCart o WHERE o.cart.id =:cartId", SubItemCart.class)
                    .setParameter("cartId", cartId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public OrderedItem getOrderedItemByBatchId(String cartId) {
        OrderedItem oi = null;
        try {
            oi = getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.cart =:cartId ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("cartId", cartId).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();

        }
        return oi;
    }

    public PaymentProvider getEnabledPaymentProvider() {
        PaymentProvider pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM PaymentProvider o WHERE o.status = TRUE", PaymentProvider.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();

        }
        return pp;

    }

    public PaymentTransaction getPaymentTransactionByTrxRef(String transRef) {
        PaymentTransaction c;
        try {
            c = getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.transactionRef =:transRef", PaymentTransaction.class)
                    .setParameter("transRef", transRef)
                    .getSingleResult();
            return c;
        } catch (Exception e) {
            return null;
        }
    }

    public PaymentTransaction getPaymentTransactionByOrder(OrderedItem oi) {
        PaymentTransaction c;
        try {
            c = getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem =:oi", PaymentTransaction.class)
                    .setParameter("oi", oi)
                    .getSingleResult();
            return c;
        } catch (Exception e) {
            return null;
        }
    }

    public List<PaymentTransaction> getPaymentTransactionsByOrder(OrderedItem oi) {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem =:oi", PaymentTransaction.class)
                    .setParameter("oi", oi).getResultList();
        } catch (Exception e) {
            System.err.println("getPaymentTransactionsByOrder ::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItemsByBusinessOwnerId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk.owner.id=:id ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getOrderedItemsByBusinessOwnerId ::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getLazyOrderedItemsByBusinessOwnerId(Long id, int start, int max) {
        System.out.println("Query Print ::::::::::: SELECT o FROM OrderedItem o WHERE o.kiosk.owner.id=" + id);
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk.owner.id=:id ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("id", id).setFirstResult(start)
                    .setMaxResults(max)
                    .getResultList();
        } catch (Exception e) {
            System.err.println("getOrderedItemsByBusinessOwnerId ::::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItems() {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o ORDER BY o.createdDate ASC", OrderedItem.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getLazyOrderedItems(int start, int max) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setFirstResult(start)
                    .setMaxResults(max)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getLazyOrderedItemsByKioskId(Long kioskId, int start, int max) {
        try {

//            return (null==kioskId) ?
//                    getEm().createQuery("SELECT o FROM OrderedItem o").getResultList() :
//                    getEm().createQuery("SELECT COUNT o FROM OrderedItem o WHERE o.kiosk.id=:id").setParameter("id", kioskId).getResultList();
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk.id=:kioskId ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("kioskId", kioskId)
                    .setFirstResult(start)
                    .setMaxResults(max)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getLazyOrderedItemsCount(Long kioskId) {
        try {

            return (Long) ((null == kioskId)
                    ? getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o").getSingleResult()
                    : getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o WHERE o.kiosk.id=:id").setParameter("id", kioskId).getSingleResult());

        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public Long getLazyOrderedItemsByBusinessOwnerCount(Long id) {
        try {

            return (Long) ((null == id)
                    ? getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o").getSingleResult()
                    : getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o WHERE o.kiosk.owner.id=:id").setParameter("id", id).getSingleResult());

        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public Long getLazyKioskOrderedItemsCount(Long kioskId) {
        try {

            return (Long) ((null == kioskId)
                    ? getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o").getSingleResult()
                    : getEm().createQuery("SELECT COUNT(o) FROM OrderedItem o WHERE o.kiosk.id=:id").setParameter("id", kioskId).getSingleResult());

        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public List<KioskPaymentOption> getKioskPaymentOptionByKioskId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM KioskPaymentOption o WHERE o.kiosk.id=:id ", KioskPaymentOption.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Paystack getPaystackInfo(String name) {
        Paystack c;
        try {
            c = getEm().createQuery("SELECT o FROM Paystack o WHERE o.name =:name", Paystack.class)
                    .setParameter("name", name)
                    .getSingleResult();
            return c;
        } catch (Exception e) {
            return null;
        }
    }

    public List<OrderedItem> getOrderedItemByKiosk(Kiosk k1) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk =:k1 ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("k1", k1).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItemByKiosk(Kiosk k1, Date dateFrom) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk =:k1 AND o.lastModifiedDate >=:dateFrom ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("k1", k1)
                    .setParameter("dateFrom", dateFrom)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItemByStage(KioskUser ku, OrderedItem.Status status) {
        try {
            return getEm().createQuery("SELECT o.item FROM OrderedItemStatusLog o WHERE o.nextUpdateBy =:ku AND o.orderStatus=:status ORDER BY o.createdDate ASC", OrderedItem.class)
                    .setParameter("ku", ku)
                    .setParameter("status", status)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItemByStage(OrderedItem.Status status) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.orderStatus=:status", OrderedItem.class)
                    .setParameter("status", status)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Cart> getCartItemsByBatchIdAndBusinessId(String batchId, Long businessId) {
        try {
            return getEm().createQuery("SELECT o.cart FROM OrderedItem o WHERE o.cart.batchId=:batchId AND o.kiosk.business.id=:businessId", Cart.class)
                    .setParameter("batchId", batchId)
                    .setParameter("businessId", businessId).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PaymentProvider getPaymentProviderByClassName(String className) {
        PaymentProvider pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM PaymentProvider o WHERE o.className=:className", PaymentProvider.class)
                    .setParameter("className", className).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public Paystack getPaystackByName(String name) {
        Paystack pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM Paystack o WHERE o.name=:name", Paystack.class)
                    .setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<OrderedItem> getAllOderedItems() {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o ORDER BY o.createdDate ASC", OrderedItem.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<BusinessActivityLog> getBusinessActivityLog(Long businessId) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessActivityLog o WHERE o.businessId=:id", BusinessActivityLog.class)
                    .setParameter("id", businessId).getResultList();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<KioskActivityLog> getKioskActivityLog(Long kioskId) {
        try {
            return getEm().createQuery("SELECT o FROM KioskActivityLog o WHERE o.id =:kioskId", KioskActivityLog.class)
                    .setParameter("id", kioskId).getResultList();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public Paystack getPayStackInfo(String morph) {
        Paystack pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM Paystack o WHERE o.name=:morph", Paystack.class)
                    .setParameter("morph", morph).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<OrderedItemStatusLog> getOrderLog(Long orderId) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItemStatusLog o WHERE o.item.id=:orderId", OrderedItemStatusLog.class)
                    .setParameter("orderId", orderId)
                    .getResultList();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    //Portal Page
    public PortalPage getPortalPagesNameByPageNameAndOutcome(String pageName, String pageOutcome) {
        PortalPage pp;
        try {
            pp = (PortalPage) getEm().createQuery("SELECT o FROM PortalPage o WHERE o.pageName=:pageName AND o.pageOutcome=:pageOutcome")
                    .setParameter("pageName", pageName).setParameter("pageOutcome", pageOutcome).getSingleResult();
        } catch (Exception e) {
            pp = null;
            System.err.println("Error :::: " + e.getLocalizedMessage());
        }
        return pp;
    }

    public PortalPermission getPortalPermissionByPermissionName(String permissionName) {
        PortalPermission pp;
        try {
            pp = (PortalPermission) getEm().createQuery("SELECT o FROM PortalPermission o WHERE o.permissionName=:permissionName")
                    .setParameter("permissionName", permissionName).getSingleResult();
        } catch (Exception e) {
            pp = null;
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<PortalPermission> getAllPortalPermission() {
        try {
            return getEm().createQuery("SELECT o FROM PortalPermission o").getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalUserGroup getPortalUserGroupByUserGroupName(String groupName) {
        PortalUserGroup group;
        try {
            group = (PortalUserGroup) getEm().createQuery("SELECT o FROM PortalUserGroup o WHERE o.userGroupName=:groupName")
                    .setParameter("groupName", groupName).getSingleResult();

        } catch (Exception e) {
            group = null;
        }
        return group;
    }

    public PortalPermission getUserPagePermissionWithPageViewId(String permissionName) {
        PortalPermission pp;
        System.out.println("SELECT o FROM PortalPermission o WHERE o.permissionName=:" + permissionName);
        try {
            pp = getEm().createQuery("SELECT o FROM PortalPermission o WHERE o.permissionName=:permissionName", PortalPermission.class)
                    .setParameter("permissionName", permissionName).getSingleResult();
        } catch (Exception e) {
            pp = null;
            System.err.println("getUserPagePermissionWithPageViewId ::: " + e.getLocalizedMessage());
        }
        return pp;
    }

    public List<PortalAccessGroup> getAllAccessGroups() {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroup o").getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalUserGroup> getAllUserGroups() {
        try {
            return getEm().createQuery("SELECT o FROM PortalUserGroup o").getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalAccessGroup_Permission> getPortalAccessGroupPermissionByAccessGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroup_Permission o WHERE o.accessGroup.id=:id")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalAccessGroup_Permission> getPortalAccessGroupPermissions() {
        try {
            return getEm().createQuery("SELECT o FROM PortalAccessGroup_Permission o").getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalAccessGroup_Permission getAccessGroupPermissionByPermissionAndAccessGroup(Long permissionId, Long accessGroupId) {
        PortalAccessGroup_Permission pagp;
        try {
            pagp = (PortalAccessGroup_Permission) getEm().createQuery("SELECT o FROM PortalAccessGroup_Permission o WHERE o.permission.id=:permissionId AND o.accessGroup.id=:accessGroupId", PortalAccessGroup_Permission.class)
                    .setParameter("permissionId", permissionId).setParameter("accessGroupId", accessGroupId).getSingleResult();
        } catch (Exception e) {
            System.err.println("getAccessGroupPermissionByPermissionAndAccessGroup Error ::: " + e.getLocalizedMessage());
            pagp = null;
        }
        return pagp;
    }

    public List<KioskItem> getAllKioskItems() {
        try {
            return getEm().createQuery("SELECT o FROM KioskItem o", KioskItem.class).getResultList();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItemStatusLog> getOrderedItemStatusLogByItemId(Long itemId) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItemStatusLog o WHERE o.item.id=:itemId ORDER BY o.createdDate ASC", OrderedItemStatusLog.class)
                    .setParameter("itemId", itemId)
                    .getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public PushRegister getPushRegisterByKioskUser(KioskUser kioskUser) {
        PushRegister pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM PushRegister o WHERE o.userId=:kioskUser", PushRegister.class)
                    .setParameter("kioskUser", kioskUser)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public BusinessStaff getBusinessStaffByKioskUser(KioskUser ku) {
        BusinessStaff pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM BusinessStaff o WHERE o.staff=:ku", BusinessStaff.class)
                    .setParameter("ku", ku)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<BusinessStaffRole> getBusinessStaffRoleByBusinessStaff(BusinessStaff bs) {
        List<BusinessStaffRole> pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff=:bs", BusinessStaffRole.class)
                    .setParameter("bs", bs)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<BusinessStaffRole> getUserRole(KioskUser kioskUser, String usergroup) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff.staff=:kioskUser AND o.userGroup.userGroupName=:usergroup", BusinessStaffRole.class)
                    .setParameter("kioskUser", kioskUser)
                    .setParameter("usergroup", usergroup)
                    .getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public List<BusinessStaffRole> getBusinessStaffRolesByKiosk(Kiosk k) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.kiosk=:kiosk", BusinessStaffRole.class)
                    .setParameter("kiosk", k)
                    .getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public List<KioskUser> getUserByKioskAndUsergroup(Kiosk kiosk, String usergroup) {
        try {
            return getEm().createQuery("SELECT o.businessStaff.staff FROM BusinessStaffRole o WHERE o.kiosk=:kiosk AND o.userGroup.userGroupName=:usergroup", KioskUser.class)
                    .setParameter("kiosk", kiosk)
                    .setParameter("usergroup", usergroup)
                    .getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public OrderedItemStatusLog getLogByOrderItemAndStatus(OrderedItem oi, String status) {
        OrderedItemStatusLog log = null;
        OrderedItem.Status s = OrderedItem.Status.valueOf(status.toUpperCase());
        try {
            log = getEm().createQuery("SELECT o FROM OrderedItemStatusLog o WHERE o.item=:oi AND o.orderStatus=:status", OrderedItemStatusLog.class)
                    .setParameter("oi", oi)
                    .setParameter("status", s)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return log;
    }

    public List<PaymentTransaction> getPortalPaymentTransactions(Long businessOwnerId, int first, int size) {
        try {
            return (null == businessOwnerId) ? getEm().createQuery("SELECT o FROM PaymentTransaction o ", PaymentTransaction.class)
                    .setFirstResult(first).setMaxResults(size).getResultList()
                    : getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem.kiosk.owner.id=:businessOwnerId", PaymentTransaction.class)
                            .setParameter("businessOwnerId", businessOwnerId).setFirstResult(first).setMaxResults(size).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getLazyPaymentTransactions(Long businessOwnerId, int first, int size, Map<String, Object> filter) {
        try {

            String queryStr = (null == businessOwnerId) ? "SELECT o FROM PaymentTransaction o"
                    : "SELECT o FROM PaymentTransaction o WHERE o.orderItem.kiosk.owner.id=:businessOwnerId";
            if (queryStr.contains("WHERE")) {
                for (Map.Entry<String, Object> entry : filter.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();

                    if (value != null) {
                        queryStr += " and o." + key + " LIKE '%" + value + "%'";
                    }
                }

                System.out.println("String Query :::::: " + queryStr);
                return getEm().createQuery(queryStr, PaymentTransaction.class)
                        .setParameter("businessOwnerId", businessOwnerId).setFirstResult(first).setMaxResults(size).getResultList();
            } else {

                for (Map.Entry<String, Object> entry : filter.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();

                    if (value != null && !queryStr.contains("WHERE")) {
                        queryStr += " WHERE o." + key + " LIKE '%" + value + "%'";
                    } else if (value != null && queryStr.contains("WHERE")) {
                        queryStr += " and o." + key + " LIKE '%" + value + "%'";
                    }
                }
                System.out.println("String Query :::::: " + queryStr);
                return getEm().createQuery(queryStr, PaymentTransaction.class).setFirstResult(first).setMaxResults(size).getResultList();
            }
//            
//            return (null == businessOwnerId) ? getEm().createQuery("SELECT o FROM PaymentTransaction o", PaymentTransaction.class)
//                    .setFirstResult(first).setMaxResults(size).getResultList() :
//                    getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem.kiosk.owner.id=:businessOwnerId", PaymentTransaction.class)
//                    .setParameter("businessOwnerId", businessOwnerId).setFirstResult(first).setMaxResults(size).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getPortalPaymentTransactionsCount(Long businessOwnerId) {
        try {

            return (null == businessOwnerId) ? getEm().createQuery("SELECT COUNT(o) FROM PaymentTransaction o", Long.class).getSingleResult()
                    : getEm().createQuery("SELECT COUNT(o) FROM PaymentTransaction o WHERE o.orderItem.kiosk.owner.id=:businessOwnerId", Long.class)
                            .setParameter("businessOwnerId", businessOwnerId).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public List<BusinessStaffRole> getBusinessStaffRoleByStaff(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff.staff.id=:id ORDER BY o.business.name, o.kiosk.name, o.userGroup.userGroupName", BusinessStaffRole.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalUser getPortalUserByKioskUser(KioskUser bs) {
        PortalUser pp = null;
        try {
            pp = getEm().createQuery("SELECT o FROM PortalUser o WHERE o.user =:bs", PortalUser.class)
                    .setParameter("bs", bs)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return pp;
    }

    public List<ItemVariation> getActiveItemVariationsByItemId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM ItemVariation o WHERE o.item.id=:id AND o.active = TRUE", ItemVariation.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<ItemVariation> getItemVariationsByItemId(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM ItemVariation o WHERE o.item.id=:id", ItemVariation.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SubItem> getSubItemsFromSubItemGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o.subItem FROM SubitemgroupSubitem o WHERE o.subItemGroup.id=:id", SubItem.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getAllSuccessfulTransaction() {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.tractionStatus=:status ORDER BY o.id DESC", PaymentTransaction.class)
                    .setParameter("status", PaymentTransaction.PaymentTractionStatus.PAYMENYSUCCESSFUL)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getTransactionBySettlement(Settlement s) {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.settlement=:settlement", PaymentTransaction.class)
                    .setParameter("settlement", s)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getAllSuccessfulTransactionByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.tractionStatus=:status AND o.orderItem.kiosk.business=:business ORDER BY o.id DESC", PaymentTransaction.class)
                    .setParameter("status", PaymentTransaction.PaymentTractionStatus.PAYMENYSUCCESSFUL)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Settlement> getAllSettlements() {
        try {
            return getEm().createQuery("SELECT o FROM Settlement o", Settlement.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Settlement> getAllSettlementsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM Settlement o WHERE o.business=:business", Settlement.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Settlement> getAllPendingSettlements() {
        try {
            return getEm().createQuery("SELECT o FROM Settlement o WHERE o.paid = FALSE", Settlement.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Settlement> getAllPendingSettlementsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM Settlement o WHERE o.paid = FALSE AND o.business=:business", Settlement.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<SplitLog> getSplitLogBySettlement(Settlement s) {
        try {
            return getEm().createQuery("SELECT o FROM SplitLog o WHERE o.settlement=:log", SplitLog.class)
                    .setParameter("log", s)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getTransactionsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem.kiosk.business=:business ORDER BY o.id DESC", PaymentTransaction.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Refund> getRefundsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM Refund o WHERE o.business=:business ORDER BY o.id DESC", Refund.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<BusinessAccount> getActiveBusinessAccountsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessAccount o WHERE o.business=:business AND o.active = TRUE ORDER BY o.id DESC", BusinessAccount.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<BusinessAccount> getBusinessAccountsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessAccount o WHERE o.business=:business ORDER BY o.id DESC", BusinessAccount.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<PaymentTransaction> getTransactionsByKiosk(Kiosk kiosk) {
        try {
            return getEm().createQuery("SELECT o FROM PaymentTransaction o WHERE o.orderItem.kiosk=:kiosk ORDER BY o.id DESC", PaymentTransaction.class)
                    .setParameter("kiosk", kiosk)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Coupon> getCouponsByCreateDate(Date date) {
        try {
            return getEm().createQuery("SELECT o FROM Coupon o WHERE o.dateGenerated=:date ORDER BY o.used DESC", Coupon.class)
                    .setParameter("date", date)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Coupon> getAllCoupons() {
        try {
            return getEm().createQuery("SELECT o FROM Coupon o ORDER BY o.used DESC", Coupon.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<Coupon> getAllCouponsByBatch(CouponBatch batch) {
        try {
            return getEm().createQuery("SELECT o FROM Coupon o WHERE o.couponBatch=:batch ORDER BY o.used DESC", Coupon.class)
                    .setParameter("batch", batch)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<CouponBatch> getAllCouponBatchesByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM CouponBatch o WHERE o.business=:business", CouponBatch.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public DistanceCost getDistanceCostByDistanceAndKiosk(Kiosk kiosk, Double distance) {
        DistanceCost ss = null;
        try {
            ss = getEm().createQuery("SELECT o FROM DistanceCost o WHERE o.kiosk=:kiosk AND o.distance=:distance", DistanceCost.class)
                    .setParameter("kiosk", kiosk)
                    .setParameter("distance", distance)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return ss;
    }

    public DistanceCost getCostByDistanceAndKiosk(Kiosk kiosk, Double distance) {
        List<DistanceCost> dcs;
        try {
            dcs = getEm().createQuery("SELECT o FROM DistanceCost o WHERE o.kiosk=:kiosk AND o.distance >=:distance  ORDER BY o.distance ASC", DistanceCost.class)
                    .setParameter("kiosk", kiosk)
                    .setParameter("distance", distance)
                    .getResultList();
            if (!dcs.isEmpty()) {
                return dcs.get(0);
            } else {
                dcs = getEm().createQuery("SELECT o FROM DistanceCost o WHERE o.kiosk=:kiosk AND o.distance <=:distance  ORDER BY o.distance DESC", DistanceCost.class)
                        .setParameter("kiosk", kiosk)
                        .setParameter("distance", distance)
                        .getResultList();
                return dcs.get(0);
            }

        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        }
    }

    public List<OrderedItem> getOrderedItemsByBusiness(Business business) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk.business=:business ORDER BY o.lastModifiedDate DESC", OrderedItem.class)
                    .setParameter("business", business)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<OrderedItem> getOrderedItemsByKiosk(Kiosk kiosk) {
        try {
            return getEm().createQuery("SELECT o FROM OrderedItem o WHERE o.kiosk=:kiosk ORDER BY o.lastModifiedDate DESC", OrderedItem.class)
                    .setParameter("kiosk", kiosk)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public List<DistanceCost> getDistanceCostsByKiosk(Kiosk kiosk) {
        try {
            return getEm().createQuery("SELECT o FROM DistanceCost o WHERE o.kiosk=:kiosk ORDER BY o.distance ASC", DistanceCost.class)
                    .setParameter("kiosk", kiosk)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Settings getSettingByName(String name) {
        Settings ss = null;
        try {
            ss = getEm().createQuery("SELECT o FROM Settings o WHERE o.name=:name", Settings.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return ss;
    }

    public Bank getBankByName(String name) {
        Bank ss = null;
        try {
            ss = getEm().createQuery("SELECT o FROM Bank o WHERE o.name=:name", Bank.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return ss;
    }

    /**
     *
     * @param lat1 Latitude of point 1 (in decimal degrees)
     * @param lon1 Longitude of point 1 (in decimal degrees)
     * @param lat2 Latitude of point 2 (in decimal degrees)
     * @param lon2 Longitude of point 2 (in decimal degrees)
     * @param unit where: 'M' is statute miles (default) , 'K' is kilometers,
     * 'N' is nautical miles
     * @return
     */
    public Double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit.equalsIgnoreCase("K")) {
            dist = dist * 1.609344;
        } else if (unit.equalsIgnoreCase("N")) {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
 /*::	This function converts decimal degrees to radians						 :*/
 /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
 /*::	This function converts radians to decimal degrees						 :*/
 /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

}
