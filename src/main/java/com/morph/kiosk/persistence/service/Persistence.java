/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.Remote;

/**
 *
 * @author root
 */
@Remote
public interface Persistence extends Serializable{

    public <T> T create(T t);

    public <T> T find(Class<T> type, Object id);

    public <T> void delete(T t);

    public <T> T update(T t);

    public List findWithNamedQuery(String queryName);

    public List findWithNamedQuery(String queryName, int resultLimit);

    public List findWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters);

    public List findWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters,
            int resultLimit);
    
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int first, int resultLimit);

    public <T> List<T> findWithNativeQuery(String sql);

    public <T> List<T> findWithNativeQuery(String sql, int resultLimit);

    public <T> T findSingleWithNativeQuery(String sql);

    
}
