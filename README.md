**CHEF TENT RESTful SERVICE PROJECT**
This EJB project is a where all the business methods for both RESTful Webservice and Chef Tent Portal reside. 

Chef Tent is an an e-commerce application that allows you the flexibility of customizing your food or soup. Should you want your meal prepared with a special spice, just add a note to the chef for your special request.

With Chef Tent, you may choose to pick up your food at your favorite kitchen or have it delivered right to your door. It also afford has the ability to schedule your orders.